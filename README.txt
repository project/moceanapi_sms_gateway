--------------------------------------------------------------------------------
                                 Mocean
--------------------------------------------------------------------------------

Project homepage: https://www.drupal.org/project/moceanapi_sms_gateway

Description
===========
This module provides integration between Mocean SMS service and Drupal SMS framework project.

Dependencies
============
1.Drupal SMS Framework module.
2.Requires Mocean subscription.(https://www.moceanapi.com)

Installation
============
1.Download and install the module as usual.
2.Go to "admin/smsframework/gateways/mocean_sms_gateway" and configure your API key, secret code of your Mocean subscription.